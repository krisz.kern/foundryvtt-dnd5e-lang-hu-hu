# FoundryVTT dnd5e lang hu-HU

## **HU**

Ez a modul a DND5e Játék Rendszerhez fordít le szövegeket és kifejezéseket magyarra.

### **Telepítés**

A FoundryVTT Admin felületén telepítsd a következő modulokat:

- [Hungarian [Core]](https://gitlab.com/krisz.kern/foundryvtt-lang-hu-hu)
- libWrapper
- babele
- **Hungarian \[DND5E\]**

A FoundryVTT Konfiguráció menüjében válaszd ki a **"Magyar (Hungarian) - Hungarian \[Core\]"** _Alapértelmezett Nyelvet_.

Mentsd el a beállítást és indítsd újra a szervert! Készen is vagy.

---

## **EN**

This module translates text and expressions to Hungarian for the DND5e Game System.

### **Installation**

Install the following modules in the Foundry Admin interface:

- [Hungarian [Core]](https://gitlab.com/krisz.kern/foundryvtt-lang-hu-hu)
- libWrapper
- babele
- **Hungarian \[DND5E\]**

In the FoundryVTT Configuration menu choose the **"Magyar (Hungarian) - Hungarian \[Core\]"** _Default Language_.

Save the setting and restart the server! That's it.
