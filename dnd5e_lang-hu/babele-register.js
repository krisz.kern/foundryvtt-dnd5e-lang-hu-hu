var creatureTypes = {
  "aberration (shapechanger)": "aberráció (alakváltó)",
  "aberration": "aberráció",
  "beast": "vadállat",
  "celestial (titan)": "égi (titán)",
  "celestial": "égi",
  "construct": "szerkezet",
  "dragon": "sárkány",
  "elemental": "elementál",
  "fey": "fey",
  "fiend (demon)": "ördögfajzat (démon)",
  "fiend (demon, orc)": "ördögfajzat (démon, ork)",
  "fiend (demon, shapechanger)": "ördögfajzat (démon, alakváltó)",
  "fiend (devil)": "ördögfajzat (ördög)",
  "fiend (devil, shapechanger)": "ördögfajzat (ördög, alakváltó)",
  "fiend (gnoll)": "ördögfajzat (gnoll)",
  "fiend (shapechanger)": "ördögfajzat (alakváltó)",
  "fiend (yugoloth)": "ördögfajzat (yugoloth)",
  "fiend": "ördögfajzat",
  "giant (cloud giant)": "óriás (felhőóriás)",
  "giant (fire giant)": "óriás (tűzóriás)",
  "giant (frost giant)": "óriás (fagyóriás)",
  "giant (hill giant)": "óriás (dombóriás)",
  "giant (stone giant)": "óriás (kőóriás)",
  "giant (storm giant)": "óriás (viharóriás)",
  "giant": "óriás",
  "humanoid (aarakocra)": "humanoid (aarakocra)",
  "humanoid (any race)": "humanoid (bármilyen fajú)",
  "humanoid (bullywug)": "humanoid (bullywug)",
  "humanoid (dwarf)": "humanoid (törpe)",
  "humanoid (elf)": "humanoid (elf)",
  "humanoid (firenewt)": "humanoid (tűzgőte)",
  "humanoid (gith)": "humanoid (gith)",
  "humanoid (gnoll)": "humanoid (gnoll)",
  "humanoid (gnome)": "humanoid (gnóm)",
  "humanoid (goblinoid)": "humanoid (goblinoid)",
  "humanoid (grimlock)": "humanoid (grimlock)",
  "humanoid (grung)": "humanoid (grung)",
  "humanoid (human)": "humanoid (ember)",
  "humanoid (human, shapechanger)": "humanoid (humain, alakváltó)",
  "humanoid (kenku)": "humanoid (kenku)",
  "humanoid (kobold)": "humanoid (kobold)",
  "humanoid (kuo-toa)": "humanoid (kuo-toa)",
  "humanoid (lizardfolk)": "humanoid (gyíknépség)",
  "humanoid (merfolk)": "humanoid (merfolk)",
  "humanoid (orc)": "humanoid (ork)",
  "humanoid (quaggoth)": "humanoid (quaggoth)",
  "humanoid (sahuagin)": "humanoid (sahuagin)",
  "humanoid (shapechanger)": "humanoid (alakváltó)",
  "humanoid (thri-kreen)": "humanoid (thri-kreen)",
  "humanoid (troglodyte)": "humanoid (troglodyte)",
  "humanoid (xvart)": "humanoid (xvart)",
  "humanoid (yuan-ti)": "humanoid (yuan-ti)",
  "humanoid": "humanoid",
  "monstrosity (shapechanger)": "szörnyeteg (alakváltó)",
  "monstrosity (shapechanger, yuan-ti)": "szörnyeteg (alakváltó, yuan-ti)",
  "monstrosity (titan)": "szörnyeteg (titán)",
  "monstrosity": "szörnyeteg",
  "ooze": "kocsonya",
  "plant": "növény",
  "swarm of Tiny beasts": "Apró termetű vadállatok raja",
  "undead (shapechanger)": "élőhalott (alakváltó)",
  "undead": "élőhalott",
};

var alignments = {
  "chaotic evil": "Kaotikus Gonosz",
  "chaotic neutral": "Kaotikus Semleges",
  "chaotic good": "Kaotikus Jó",
  "neutral evil": "Semleges Gonosz",
  "true neutral": "Semleges",
  "neutral": "Semleges",
  "neutral good": "Semleges Jó",
  "lawful evil": "Törvényes Gonosz",
  "lawful neutral": "Törvényes Semleges",
  "lawful good": "Törvényes Jó",
  "chaotic good evil": "Kaotikus Jó/Gonosz",
  "lawful chaotic evil": "Törvényes/Kaotikus Gonosz",
  "unaligned": "Értékrend nélküli",
};

var replaceLanguages = {
  "giant eagle": "óriássas",
  "worg": "worg",
  "winter wolf": "fehér farkas",
  "sahuagin": "sahuagin",
  "giant owl, understands but cannot speak all but giant owl":
    "óriásbagoly, megérti, de csak óriásbagoly nyelven képes beszélni",
  "giant elk but can't speak them": "óriás jávorszarvas nyelv, de nem képes beszélni",
  "understands infernal but can't speak it": "megérti az ördögi nyelvet, de nem képes beszélni",
  "understands draconic but can't speak": "megérti az sárkánynyelvet, de nem képes beszélni",
  "understands common but doesn't speak it": "megérti a közöst, de nem képes beszélni",
  "understands abyssal but can't speak": "megérti az abyssal nyelvet, de nem képes beszélni",
  "understands all languages it knew in life but can't speak":
    "minden nyelvet megért, amit életében ismert, de nem képes beszélni",
  "understands commands given in any language but can't speak":
    "bármilyen nyelven megért parancsokat, de nem képes beszélni",
  "(can't speak in rat form)": "(nem képes patkány formában beszélni)",
  "(can't speak in boar form)": "(nem képes vaddisznó formában beszélni)",
  "(can't speak in bear form)": "(nem képes medve formában beszélni)",
  "(can't speak in tiger form)": "(nem képes tigris formában beszélni)",
  "any one language (usually common)": "bármely 1 nyelv (általában közös)",
  "any two languages": "bármely 2 nyelv",
  "any four languages": "bármely 4 nyelv",
  "5 other languages": "5 másik nyelv",
  "any, usually common": "bármelyik, általában a közös",
  "one language known by its creator": "egy nyelv, amit az alkotója ismert",
  "the languages it knew in life": "az életében ismert nyelvek",
  "those it knew in life": "az életében ismert nyelvek",
  "all it knew in life": "az életében ismert nyelvek",
  "any it knew in life": "az életében ismert nyelvek",
  "all, telepathy 120 ft.": "minden, telepátia 36m",
  "telepathy 60 ft.": "telepátia 18m",
  "telepathy 60ft. (works only with creatures that understand abyssal)":
    "telepátia 18m (csak az abyssal nyelvet értő lényekkel működik)",
  "telepathy 120 ft.": "telepátia 36m",
  "but can't speak": "de nem képes beszélni",
  "but can't speak it": "de nem beszéli",
  "choice": "választás",
  "understands the languages of its creator but can't speak":
    "megérti az alkotója nyelvét, de nem képes beszélni",
  "understands common and giant but can't speak":
    "megérti a közös és óriás nyelvet, de nem képes beszélni",
  "cannot speak": "nem képes beszélni",
};

var races = {
  "Dragonborn": "Sárkányszülött",
  "Dwarf": "Törpe",
  "Hill Dwarf": "Nano delle Colline",
  "Elf": "Elf",
  "High Elf": "Nemes Elf",
  "Gnome": "Gnóm",
  "Rock Gnome": "Szikla Gnóm",
  "Half Elf": "Fél-elf",
  "Half-Elf": "Fél-elf",
  "Half-elf": "Fél-elf",
  "Halfling": "Félszerzet",
  "Lightfoot Halfling": "Könnyűléptű Félszerzet",
  "Half Orc": "Fél-ork",
  "Half-Orc": "Fél-ork",
  "HUMAN": "Ember",
  "Human": "Ember",
  "Variant Human": "Ember Változat",
  "Tiefling": "Tiefling",
};

var classes = {
  "Barbarian": "Barbár",
  "Bard": "Bárd",
  "Cleric": "Pap",
  "Druid": "Druida",
  "Fighter": "Harcos",
  "Monk": "Szerzetes",
  "Paladin": "Paladin",
  "Ranger": "Kósza",
  "Rogue": "Zsivány",
  "Sorcerer": "Mágus",
  "Warlock": "Boszorkány",
  "Wizard": "Varázsló",
  "College of Lore": "A Tudás Iskolája",
  "Oath of Devotion": "Odaadás Esküje",
  "Life Domain": "Élet Szféra",
  "Circle of the Land": "A Vidék Köre",
  "The Fiend": "Az Ördögfajzat",
  "Hunter": "Vadász",
  "School of Evocation": "Evokáció Iskolája",
  "Path of the Berserker": "A Berzerker Útja",
  "Eldritch Blast": "Eldritch Csapás",
  "Pact of the Tome": "A Kötet Paktuma",
  "Pact of the Blade": "A Penge Paktuma",
  "Pact of the Chain": "A Lánc Paktuma",
  "Way of the Open Hand": "A Nyitott Kéz Útja",
};

var rarity = {
  "Common": "Gyakori",
  "Uncommon": "Nem Gyakori",
  "Rare": "Ritka",
  "Very rare": "Nagyon Ritka",
  "Legendary": "Legendás",
};

function replaceSenses(chain) {
  var regexp = /([0-9]+)/gi;
  chain = chain.replace(/ft/gi, "m");
  chain = chain.replace(/feet/gi, "m");
  chain = chain.replace(/Darkvision/gi, "Sötétlátás");
  chain = chain.replace(/Darvision/gi, "Sötétlátás"); //bug ^^
  chain = chain.replace(/Blindsight/gi, "Vaklátás");
  chain = chain.replace(/Truesight/gi, "Igazlátás");
  chain = chain.replace(/tremorsense/gi, "Rezgésérzék");
  chain = chain.replace(/Blind Beyond/gi, "ezen túl nem");
  chain = chain.replace(/this radius/gi, "képes érzékelni");
  chain = chain.replace(chain.match(regexp), ft2m(parseFloat(chain.match(regexp))));
  chain = chain.replace("(blind beyond this radius)", "(ezen túl nem képes érzékelni)");
  return chain;
}

function replaceDamageTypes(chain) {
  chain = chain.replace(/bludgeoning/gi, "zúzó");
  chain = chain.replace(/piercing/gi, "szúró");
  chain = chain.replace(/and/gi, "és");
  chain = chain.replace(/slashing/gi, "vágó");
  chain = chain.replace(/from/gi, "amik");
  chain = chain.replace(/nonmagical attacks/gi, "nem mágikus támadásból származnak");
  chain = chain.replace(/that aren't silvered/gi, "nem ezüstözött fegyverből származnak");
  chain = chain.replace(/not made with silvered weapons/gi, "nem ezüstözött fegyverből származnak");
  return chain;
}

function ft2m(value) {
  if (!value) return value;
  return Math.round(parseFloat(value) * 0.3048 * 2) / 2;
}

function lb2kg(value) {
  if (!value) return value;
  return Math.round(parseFloat(value) * 0.453592 * 2) / 2;
}

function mile2km(value) {
  if (!value) return value;
  return parseInt(value) * 1.5;
}

function convertEnabled() {
  return game.settings.get("dnd5e_lang-hu", "convertMetric");
}

function setEncumbranceData() {
  let convert = convertEnabled();
  game.settings.set("dnd5e", "metricWeightUnits", convert);
}

Hooks.once("init", () => {
  //CONFIG.debug.hooks = true;

  game.settings.register("dnd5e_lang-hu", "convertMetric", {
    name: "Metrikus Rendszerre Konvertálás",
    hint: "Ezzel a beállítással a Modul átkonvertálja a mértékegységeket az angolszász rendszerről a metrikus rendszerre.",
    type: Boolean,
    default: false,
    scope: "world",
    config: true,
    onChange: (value) => {
      window.location.reload();
    },
  });

  if (typeof Babele !== "undefined") {
    Babele.get().register({
      module: "dnd5e_lang-hu",
      lang: "hu",
      dir: "compendium",
    });

    Babele.get().registerConverters({
      weight: (value) => {
        if (convertEnabled()) {
          return lb2kg(value);
        } else {
          return value;
        }
      },
      range: (range) => {
        if (range) {
          if (convertEnabled()) {
            if (range.units === "ft") {
              if (range.long) {
                range = mergeObject(range, { long: ft2m(range.long) });
              }
              range.units = "m";
              return mergeObject(range, { value: ft2m(range.value) });
            }
            if (range.units === "mi") {
              if (range.long) {
                range = mergeObject(range, { long: mile2km(range.long) });
              }
              range.units = "km";
              return mergeObject(range, { value: mile2km(range.value) });
            }
          }
          return range;
        }
      },
      movement: (movement) => {
        if (movement) {
          if (convertEnabled()) {
            if (movement.units === "ft") {
              for (var i in movement) {
                if (movement[i] === "ft") {
                  movement[i] = "m";
                } else {
                  movement[i] = ft2m(movement[i]);
                }
              }
            }
            if (movement.units === "mi") {
              for (var i in movement) {
                if (movement[i] === "mi") {
                  movement[i] = "km";
                } else {
                  movement[i] = mile2km(movement[i]);
                }
              }
            }
          }
          return movement;
        }
      },
      alignment: (alignment) => {
        return alignments[alignment.toLowerCase()];
      },
      type: (typeC) => {
        return creatureTypes[typeC.value.toLowerCase()];
      },
      senses: (senses) => {
        if (senses != null) {
          //console.log(JSON.parse(JSON.stringify(sens)));
          const sensesSplit = senses.split(", ");
          //console.log(JSON.parse(JSON.stringify(sensSplit)));
          var sensesTranslated = "";
          sensesSplit.forEach(function (el) {
            //console.log(JSON.parse(JSON.stringify(el)));
            sensesTranslated = replaceSenses(el) + " " + sensesTranslated;
          });
          return sensesTranslated;
        }
      },
      di: (damageTypes) => {
        return replaceDamageTypes(damageTypes);
      },
      languages: (languages) => {
        if (languages != null) {
          //console.log(JSON.parse(JSON.stringify(languages)));
          const languagesSplit = languages.split("; ");
          var languagesFinal = "";
          var languagesTranslated = "";
          languagesSplit.forEach(function (el) {
            languagesTranslated = replaceLanguages[el.toLowerCase()];
            if (languagesTranslated != null) {
              if (languagesFinal == "") {
                languagesFinal = languagesTranslated;
              } else {
                languagesFinal = languagesFinal + " ; " + languagesTranslated;
              }
            }
          });
          return languagesFinal;
        }
      },
      token: (token) => {
        if (convertEnabled()) {
          mergeObject(token, {
            dimSight: ft2m(token.dimSight),
            brightSight: ft2m(token.brightSight),
          });
        }
      },
      race: (race) => {
        return races[race] ? races[race] : race;
      },
      rarity: (r) => {
        return rarity[r] ? rarity[r] : r;
      },
      raceRequirements: (requirements) => {
        let translated = requirements;
        const names = Object.keys(races);
        names.forEach((name) => {
          translated = translated.replaceAll(name, races[name]);
        });
        return translated;
      },
      classRequirements: (requirements) => {
        let translated = requirements;
        const names = Object.keys(classes);
        names.forEach((name) => {
          translated = translated.replaceAll(name, classes[name]);
        });
        return translated;
      },
      classNameFormula: (formula) => {
        if (formula && typeof formula === "string") {
          let translated = formula;
          const names = Object.keys(classes);
          names.forEach((name) => {
            translated = translated.replaceAll(name.toLowerCase(), classes[name].toLowerCase());
          });
          return translated;
        }
      },
      damagePartClassName: (array1) => {
        for (let i = 0; i < array1.length; i++) {
          let array2 = array1[i];
          for (let j = 0; j < array2.length; j++) {
            let translated = array2[j];
            if (translated && typeof translated === "string") {
              const names = Object.keys(classes);
              names.forEach((name) => {
                translated = translated.replaceAll(name.toLowerCase(), classes[name].toLowerCase());
              });
              array2[j] = translated;
            }
          }
          array1[i] = array2;
        }
        return array1;
      },
    });

    CONFIG.DND5E.classFeatures = {
      barbár: CONFIG.DND5E.classFeatures["barbarian"],
      bárd: CONFIG.DND5E.classFeatures["bard"],
      pap: CONFIG.DND5E.classFeatures["cleric"],
      druida: CONFIG.DND5E.classFeatures["druid"],
      harcos: CONFIG.DND5E.classFeatures["fighter"],
      szerzetes: CONFIG.DND5E.classFeatures["monk"],
      paladin: CONFIG.DND5E.classFeatures["paladin"],
      kósza: CONFIG.DND5E.classFeatures["ranger"],
      zsivány: CONFIG.DND5E.classFeatures["rogue"],
      mágus: CONFIG.DND5E.classFeatures["sorcerer"],
      boszorkány: CONFIG.DND5E.classFeatures["warlock"],
      varázsló: CONFIG.DND5E.classFeatures["wizard"],
    };
  }
});

Hooks.once("ready", () => {
  setEncumbranceData();
});

Hooks.on("createScene", (scene) => {
  //	console.log(JSON.parse(JSON.stringify(scene)));
  if (convertEnabled()) {
    scene.update({ gridUnits: "m", gridDistance: 1.5 });
  }
});

Hooks.on("createActor", (actor) => {
  if (actor.getFlag("babele", "translated")) return;
  if (convertEnabled()) {
    actor.update({
      token: {
        dimSight: ft2m(actor.data.token.dimSight),
        brightSight: ft2m(actor.data.token.brightSight),
      },
      data: {
        attributes: {
          movement: {
            units: "m",
            walk: ft2m(actor.data.data.attributes.movement.walk),
          },
        },
      },
    });
  }
});
